\chapter{Introduction}

As a signatory to the Convention on Biological Diversity (CBD), Canada has committed to protecting 10% of coastal and marine areas in ecologically representative and well-connected marine protected areas (MPAs), and to protect ecosystem, species, and genetic diversity (Aichi Target 11 in @cbd2011,@dfo2016).<!--fix brackets--> The Government of Canada, led by Fisheries and Oceans Canada (DFO), is working in partnership with the Government of British Columbia (BC) and 16 First Nations as part of the Marine Protected Area Technical Team (MPATT) to develop an MPA network in the Northern Shelf Bioregion (NSB; Figure \@ref(fig:map)).

The primary goal of the development of MPA networks in BC is to protect and maintain marine biodiversity, ecological representation and special natural features [@mpastrategy]. To identify areas of ecological importance within the planning area, ecological data representing conservation priorities (the species, areas, and other features of interest, @cpdoc) was used in site-selection analyses using the decision support tool Marxan. The ecological data was also used to characterize sites within draft network scenarios, which include existing protected areas, sites identified through Marxan, and proposed sites brought forward by the planning partners.


<!-- 
- This is a bullet
    - This is a sub-bullet
         - This is a sub-sub-bullet
              - Third level?
    - This is a second sub
- First level again
-->


```{r map, echo=FALSE, fig.cap="The Northern Shelf Bioregion (NSB), Pacific Region.", out.width = '20%'}
knitr::include_graphics("figures/NSB_lab1.png")
```

<!-- 
# Known issues

------------------------------------------------------
 issue                                      status
----------------------------------------- ------------
 weird placeholder sometimes                not fixed (change and save index, like a space at the bottom, then try to knit again)
 
 front page doesn't need footnotes          not fixed
 
 can only do numbered lists, not bullets    not fixed
------------------------------------------------------
-->